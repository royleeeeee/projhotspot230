/*
 Functions for maps in "result.php" and "detail.php"
*/

function initiateMap(markerArray, geoMarker, geoLat, geoLng) {
    var map = L.map('leafletMap', {center: [-27.4698, 153.0251], zoom: 10});

    for(i = 0; i < markerArray.length; i++) {
        var marker = L.marker([markerArray[i][0], markerArray[i][1]]).addTo(map);
        if (markerArray[i][3] != undefined ) {
            var popupMessage = markerArray[i][2] + "<br>" +
                "<a href='detail.php?id="+ markerArray[i][3]+"'>View Details</a>";
        } else {
            var popupMessage = markerArray[i][2];
        }
        marker.bindPopup(popupMessage);
    }

    L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=' +
            'pk.eyJ1Ijoicm95bGVlZWVlZSIsImEiOiJjamhobHhyNDIwcHUyM2NrM2Q1eDBlOW85In0.IHNlP9f2KtRSGIRoTM5sFA', {
            maxZoom: 18,
            id: 'mapbox.streets',
            accessToken: 'your.mapbox.access.token'
        }).addTo(map);

    if (geoMarker) {
        var geoIcon = L.icon({
            iconUrl: '../public/image/geoIcon.png',
            iconAnchor: [12, 40],
            popupAnchor: [0, -35]
        });
        var marker = L.marker([geoLat,geoLng], {icon: geoIcon}).addTo(map);
        marker.bindPopup("Current Location");
    }
}

function geoLocate() {
	if (navigator.geolocation) {
		navigator.geolocation.getCurrentPosition(showPosition, showError);
	} else {
		alert("Geolocation is not supported by this browser.");
	}
}

function showPosition(position) {
	window.location.replace("result.php?geo=1/" + position.coords.latitude +
        "/" + position.coords.longitude + "/5"
    );
}

function showError(error) {
	var msg = '';
	switch(error.code) {
		case error.PERMISSION_DENIED:
			msg = 'ERROR: User denied the request for Geolocation.'
			break;
		case error.POSITION_UNAVAILABLE:
			msg = "ERROR: Location information is unavailable."
			break;
		case error.TIMEOUT:
			msg = "ERROR: The request to get user location timed out."
			break;
		case error.UNKNOWN_ERROR:
			msg = "ERROR: An unknown error occurred."
			break;
	}
	alert(msg);
}