/*
 Helper DOM methods
*/

function hideElement(elementId) {
    document.getElementById(elementId).style.display = 'none';
}

function displayElement(elementId, displayStyle) {
    document.getElementById(elementId).style.display = displayStyle;
}

function disableElement(elementId) {
    document.getElementById(elementId).disabled = true;
}

/*
 Validation for search form in "search.php"
*/

function validateSearch(form) {
    if (!checkCriteriaBlank(form)) {
        return false;
    } else {
        return true;
    }
}

function checkCriteriaBlank(form) {
    // atleast 1 search criteria is selected
    if (form.suburb.value == '' &&
        form.name.value == '' &&
        form.rating.value == '') {
        displayElement('criteriaBlank', 'block');
        return false;
    } else {
        return true;
    }
}

/*
 Validation for registration form in "register.php"
*/

function validateRegister(form) {
    if (!checkPasswordMatch(form) ||
        !checkName(form) ||
        !checkUsername(form)) {
        return false;
    } else {
        return true;
    }
}

function checkPasswordMatch(form) {
    // password matches with confirmPassword
	if (form.password.value != form.confirmPassword.value) {
        displayElement('passwordNotMatch', 'block');
		return false;
	} else {
	    return true;
    }
}

function checkName(form) {
    // return false if string contain numbers
    if (/\d/.test(form.firstName.value)) {
        displayElement('firstNameInvalid', 'block');
        return false;
    } else if (/\d/.test(form.lastName.value)) {
        displayElement('lastNameInvalid', 'block');
        return false;
    } else {
        return true;
    }
}

function checkUsername(form) {
    // return false if string contain white spaces
    if (/\s/g.test(form.username.value)) {
        displayElement('usernameInvalid', 'block');
        return false;
    } else {
        return true;
    }
}

/*
 Validation for review submission form in "detail.php"
*/

function validateReview(form) {
    if (!checkRatingBlank(form)) {
        return false;
    } else {
        return true;
    }
}

function checkRatingBlank(form) {
    // rating selection is not blank
    if (form.submitReviewRating.value == '') {
        displayElement('reviewRatingBlank', 'block');
        return false;
    } else {
        return true;
    }
}


