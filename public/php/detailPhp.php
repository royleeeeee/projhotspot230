<?php

require "../public/php/phpFunctions.php";

# redirect to "search.php" if id is blank or not set
if (!isset($_GET['id']) || empty($_GET['id'])) {
    $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/search.php";
    header("Location: ". $redirectUrl);
    exit();

} else {
    $arrayDetail = getDetailById($pdo, $_GET['id']);
    $arrayReview = getReviewById($pdo, $_GET['id']);
}

if (isset ($_POST['submitReview'])) {
    # submit user review to database
    if (!empty ($_POST['submitReviewRating'])) {
        session_start();
        if (isset($_SESSION['isUser'])) {
            $hotspotId = $_POST['id'];
            $reviewName = $_SESSION['firstName'] . ' ' . $_SESSION['lastName'];
            $reviewId = $_SESSION['userId'];
            $rating = $_POST['submitReviewRating'];
            $reviewText = htmlspecialchars($_POST['submitReviewText']);

            submitReview($pdo, $hotspotId, $reviewName, $reviewId, $rating, $reviewText);

            $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).
                "/detail.php?id=". $hotspotId;
            header("Location: ". $redirectUrl);

            exit();

        } else if (!isset($_SESSION['isUser'])) {
            exit();
        }
    } else {
        echo '<script>alert(\'Rating field is required\')</script>';
    }
}

?>