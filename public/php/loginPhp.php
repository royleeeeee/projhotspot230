<?php
require "../public/php/phpFunctions.php";

$loginStatus = true;

if (isset ($_POST['loginButton'])) {

    if (!empty ($_POST['username']) && !empty ($_POST['password'])) {

        $arrayUser = loginUser($pdo, $_POST['username'], $_POST['password']);

        if (($arrayUser)->rowCount() == 1) {
            # store user data in cookies when logged in
            session_start();

            $_SESSION['isUser'] = true;

            foreach ($arrayUser as $user) {
                $_SESSION['userId'] = $user['id'];
                $_SESSION['firstName'] = $user['firstname'];
                $_SESSION['lastName'] = $user['lastname'];
                $_SESSION['username'] = $user['username'];
            }

            $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']);
            header("Location: ".$redirectUrl);
            exit();
        } else {
            $loginStatus = false;
        }

    } else {
        echo '<script>alert(\'All fields are required\')</script>';
    }
}
?>