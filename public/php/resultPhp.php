<?php

require "../public/php/phpFunctions.php";

$geoLocate = false;
$geoLat;
$geoLng;
$distance;

if (isset($_GET['suburb'])) {
    // Pull information from database when user searched via suburb
    $arrayMarker = getResultBySuburb($pdo, $_GET['suburb']);
    $arrayResult = getResultBySuburb($pdo, $_GET['suburb']);
    $headerMessage = "Hotspot(s) in ". $_GET['suburb'];

} else if (isset($_GET['name'])) {
    // Pull information from database when user searched via location name
    $arrayMarker = getResultByName($pdo, $_GET['name']);
    $arrayResult = getResultByName($pdo, $_GET['name']);
    $headerMessage = "Hotspot(s) with the name \"". $_GET['name'] ."\"";

} else if (isset($_GET['rating'])) {
    // Pull information from database when user searched via minimum rating
    $arrayMarker = getResultByRating($pdo, $_GET['rating']);
    $arrayResult = getResultByRating($pdo, $_GET['rating']);
    $headerMessage = "Hotspot(s) with rating ". $_GET['rating'] ." or above";

} else if (isset($_GET['geo'])) {
    // Pull information from database when user searched via geolocation
    $arrayUrl = explode("/", $_GET['geo']);
    $geoLat = $arrayUrl[1];
    $geoLng = $arrayUrl[2];
    $distance = $arrayUrl[3];
    $geoLocate = true;

    $arrayMarker = getResultByGeo($pdo, $geoLat, $geoLng, $distance);
    $arrayResult = getResultByGeo($pdo, $geoLat, $geoLng, $distance);
    $headerMessage = "Hotspot(s) within ". $distance ."km";

} else {
    // Redirect to "search.php" if no url parameter is given
    $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/search.php";
    header("Location: ". $redirectUrl);
    exit();
}

// Redirect to "search.php" if SQL query returns no result
if ($arrayResult->rowCount() == 0) {
    $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).
        "/search.php?error=1";
    header("Location: ". $redirectUrl);
    exit();
}

?>