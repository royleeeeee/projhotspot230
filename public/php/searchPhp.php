<?php

require "../public/php/phpFunctions.php";

# Error message variables
$criteriaBlank = false;
$noResult = false;

$geoLocate = false;

$array = getAllSuburb($pdo); # Get a list of suburbs from database
$arraySuburb = array();
$cleanArraySuburb = array();

foreach ($array as $suburb) {
    # remove coma and postcodes from suburbs and push into a new array
    if (preg_match('/[,0-9]+/', $suburb['suburb']) > 0) {
        $suburb['suburb'] = preg_filter('/[,0-9]+/', '', $suburb['suburb']);
    }

    array_push($arraySuburb, trim($suburb['suburb']));
}

# remove duplicate entries and sort alphabetically
$cleanArraySuburb = array_unique($arraySuburb);
sort($cleanArraySuburb);

if (isset($_POST['criteriaButton'])) {
    # When "Search by Criteria" button is clicked, redirect to result page

    if (!empty($_POST['suburb'])) {
        $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).
            "/result.php?suburb=" . $_POST['suburb'];
        header("Location: ". $redirectUrl);

    } else if (!empty($_POST['name'])) {
        $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).
            "/result.php?name=" . urlencode($_POST['name']);
        header("Location: ". $redirectUrl);

    } else if (!empty($_POST['rating'])) {
        $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF']).
            "/result.php?rating=" . $_POST['rating'];
        header("Location: ". $redirectUrl);

    } else {
        $criteriaBlank = true;
    }

} else if (isset($_POST['geoButton'])) {
    # Initiate geolocation functions when "Search by Current Location" button is clicked
    $geoLocate = true;

} else if (isset($_GET['error'])) {
    # Redirect to "search.php" if SQL query returns no result
    $noResult = true;

}

?>