<?php

require "../public/php/phpFunctions.php";

$emailDuplicate = false;
$usernameDuplicate = false;
$fNameInvalid = false;
$lNameInvalid = false;
$usernameInvalid = false;

if (isset ($_POST['register'])) {

    if (!empty ($_POST['firstName']) && !empty ($_POST['lastName']) && !empty ($_POST['email']) &&
        !empty ($_POST['postCode']) && !empty ($_POST['username']) && !empty ($_POST['password'])) {

        # check user input for errors
        if (checkEmailDuplicate($pdo, $_POST['email'])) {
            $emailDuplicate = true;

        } else if (checkUsernameDuplicate($pdo, $_POST['username'])) {
            $usernameDuplicate = true;

        } else if (checkNumbersInString($_POST['firstName'])) {
            $fNameInvalid = true;

        } else if (checkNumbersInString($_POST['lastName'])) {
            $lNameInvalid = true;

        } else if (checkSpacesInString($_POST['username'])) {
            $usernameInvalid = true;

        } else {
            if (registerUser($pdo, htmlspecialchars($_POST['firstName']), htmlspecialchars($_POST['lastName']),
                    htmlspecialchars($_POST['email']), htmlspecialchars($_POST['postCode']),
                    htmlspecialchars($_POST['username']), htmlspecialchars($_POST['password']))) {

                $redirectUrl = "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/login.php";
                header("Location: ". $redirectUrl);
                exit();
            }
        }
    } else {
        echo '<script>alert(\'All fields are required\')</script>';
    }
}
?>