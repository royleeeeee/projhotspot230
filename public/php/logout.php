<?php

session_start();

unset($_SESSION['isUser']);
unset($_SESSION['userId']);
unset($_SESSION['firstName']);
unset($_SESSION['lastName']);
unset($_SESSION['username']);

$redirectUrl = ((isset($_SERVER['HTTP_REFERER'])) ?
    $_SERVER['HTTP_REFERER'] :
    "http://".$_SERVER['HTTP_HOST'].dirname($_SERVER['PHP_SELF'])."/index.php");

header('Location:'.$redirectUrl);

exit();

?>