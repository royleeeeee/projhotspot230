<?php

#pdo = new PDO('mysql:host=localhost; dbname=projhotspot', 'min', 'wasd.123');
$pdo = new PDO('mysql:host=cab230.sef.qut.edu.au; dbname=n9586881', 'n9586881', 'qwer.999');

$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

# Get a list of all suburbs in the database
function getAllSuburb ($pdo) {
    try {
        return $arraySuburb = $pdo->query('
            SELECT suburb FROM hotspotinfo;
        ');
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
};

# Get a list of hotspots in a userdefined suburb
function getResultBySuburb($pdo, $suburb) {
    try {
        $stmt = $pdo->prepare('
            SELECT id, name, address, suburb, latitude, longitude 
            FROM hotspotinfo
            WHERE suburb LIKE :suburb;
        ');
        $stmt->bindValue(':suburb', '%' . $suburb . '%');
        $stmt->execute();

        return $arrayResult = $stmt;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

# Get a list of hotspots with names containing a userdefined string
function getResultByName($pdo, $name) {
    try {
        $stmt = $pdo->prepare('
            SELECT id, name, address, suburb, latitude, longitude
            FROM hotspotinfo
            WHERE name LIKE :name;
        ');
        $stmt->bindValue(':name', '%'. $name .'%');
        $stmt->execute();

        return $arrayResult = $stmt;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

# Get a list of hotspots with a userdefined mimimum rating
function getResultByRating($pdo, $minRate) {
    try {
        $stmt = $pdo->prepare('
            SELECT id, name, address, suburb, latitude, longitude
            FROM hotspotinfo
            WHERE EXISTS ( 
                SELECT hotspotid FROM hotspotreview
                WHERE hotspotinfo.id = hotspotreview.hotspotid    
            ) AND (
                (SELECT ROUND(AVG(rating)) FROM hotspotreview
                WHERE hotspotinfo.id = hotspotreview.hotspotid) >= :minRate
            );
        ');
        $stmt->bindValue(':minRate', $minRate);
        $stmt->execute();

        return $arrayResult = $stmt;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

# Get a list of hotspots within a radius of geolocated coordinates
function getResultByGeo($pdo, $geoLat, $geoLng, $distance) {
    try {
        $stmt = $pdo->prepare('
            SELECT id, name, address, suburb, latitude, longitude,
                111.111 * 
                DEGREES(ACOS(COS(RADIANS(:geoLat)) * 
                COS(RADIANS(latitude)) * 
                COS(RADIANS(:geoLng - longitude)) + 
                SIN(RADIANS(:geoLat)) * 
                SIN(RADIANS(latitude)))) AS distance
        FROM hotspotinfo 
        HAVING distance<=:distance ORDER BY distance ASC

        ');
        $stmt->bindValue(':geoLat', $geoLat);
        $stmt->bindValue(':geoLng', $geoLng);
        $stmt->bindValue(':distance', $distance);
        $stmt->execute();

        return $arrayResult = $stmt;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

# Get the details of a hotspot
function getDetailById($pdo, $id) {
    try {
        $stmt = $pdo->prepare('
            SELECT name, address, suburb, latitude, longitude
            FROM hotspotinfo
            WHERE id = :id;
        ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        return $arrayDetail = $stmt;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

# Get a list of reviews of a hotspot
function getReviewById($pdo, $id) {
    try {
        $stmt = $pdo->prepare('
            SELECT reviewname, date, rating, content
            FROM hotspotreview
            WHERE hotspotid = :id;
        ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        return $arrayReview = $stmt;
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

# Get the average rating of a hotspot
function getAverageRatingById($pdo, $id) {
    try {
        $stmt = $pdo->prepare('
            SELECT ROUND(AVG(rating)) FROM hotspotreview
	        WHERE  hotspotreview.hotspotid = :id
        ');
        $stmt->bindValue(':id', $id);
        $stmt->execute();

        $arrayRating = $stmt;

        foreach ($arrayRating as $rating) {
            return $rating['ROUND(AVG(rating))'];
        }

    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

# Submit review to a hotspot
function submitReview($pdo, $hotspotId, $reviewName, $reviewId, $rating, $reviewText) {
    try {
        $stmt = $pdo->prepare('
            INSERT INTO hotspotreview (hotspotid, reviewname, reviewid, date, rating, content) 
            VALUES (:hotspotId, :reviewName, :reviewId, :date, :rating, :content);
        ');
        $stmt->bindValue(':hotspotId', $hotspotId);
        $stmt->bindValue(':reviewName', $reviewName);
        $stmt->bindValue(':reviewId', $reviewId);
        $stmt->bindValue(':date', date("Y-m-d"));
        $stmt->bindValue(':rating', $rating);
        $stmt->bindValue(':content', $reviewText);
        $stmt->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
}

# Register user details into the database
function registerUser($pdo, $firstName, $lastName, $email,
                      $postCode, $username, $password) {
    try {
        $stmt = $pdo->prepare('
            INSERT INTO userinfo 
            (firstname, lastname, email, postCode, username, salt, password) 
            VALUES (:firstName, :lastName, :email, :postCode, :username, 
            \'4b3403665fea6\', SHA2(CONCAT(:password, \'4b3403665fea6\'), 0)); 
        ');
        $stmt->bindValue(':firstName', $firstName);
        $stmt->bindValue(':lastName', $lastName);
        $stmt->bindValue(':email', $email);
        $stmt->bindValue(':postCode', str_pad($postCode, 4,
            "0", STR_PAD_LEFT));
        $stmt->bindValue(':username', $username);
        $stmt->bindValue(':password', $password);
        $stmt->execute();

        return true;
    } catch (PDOException $e) {
        echo $e->getMessage();

        return false;
    }
}

# Return user information if provided credentials matches with the database
function loginUser($pdo, $username, $password) {
    try {
		$stmt = $pdo->prepare('
            SELECT id, firstname, lastname, username
            FROM userinfo
            WHERE username = :username 
            AND password = SHA2(CONCAT(:password, salt), 0);	
	    ');
		$stmt->bindValue(':username', $username);
		$stmt->bindValue(':password', $password);
        $stmt->execute();

        return $arrayUser = $stmt;
	} catch (PDOException $e) {
		echo $e->getMessage();
	}
}

# Get submited value to prefill form
function posted_value($name) {
	if (isset($_POST[$name])) {
		return htmlspecialchars($_POST[$name]);
	}
}

# Check to see if provided username is already registered in the database
function checkUsernameDuplicate($pdo, $username) {
    try {
        $stmt = $pdo->prepare('
            SELECT username 
            FROM userinfo
            WHERE username = :username;
            
        ');
        $stmt->bindValue(':username', $username);
        $stmt->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    return $stmt->rowCount() > 0;
}

# Check to see if provided email is already registered in the database
function checkEmailDuplicate($pdo, $email) {
    try {
        $stmt = $pdo->prepare('
            SELECT email 
            FROM userinfo
            WHERE email = :email;            
        ');
        $stmt->bindValue(':email', $email);
        $stmt->execute();
    } catch (PDOException $e) {
        echo $e->getMessage();
    }

    return $stmt->rowCount() > 0;
}

# Returns true if a string contains numbers
function checkNumbersInString($string) {
    if (1 === preg_match('~[0-9]~', $string)) {
        return true;
    } else {
        return false;
    }
}

# Returne true of a string contains spaces
function checkSpacesInString($string) {
    if (1 === preg_match('/\s/', $string)) {
        return true;
    } else {
        return false;
    }
}

?>
