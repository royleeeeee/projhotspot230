<?php include '../public/php/registerPhp.php' ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Register - Wifi Hotspot</title>

    <?php include '../include/metadata.php'; ?>

    <link rel="stylesheet" href="../public/stylesheet/style.css"/>
    <link rel="shortcut icon" type="image/png" href="../public/image/favicon.png"/>

    <script src="../public/javascript/javascript.js"></script>

</head>

<body class='wrapperBody'>

    <?php include '../include/microdata.php' ?>

    <!-- Banner and Navigation Bar -->
    <?php
        $page = 'login';
        include '../include/navbar.php';
    ?>

    <div class='wrapperContent'>

        <article class="content">

            <!-- Page name -->
            <header><h2>Register</h2></header>

            <!-- Current directory -->
            <footer><p class='directory'>
                <a href='login.php'>Login</a> /
                Register
            </p></footer>

            <!-- Registration form -->
            <div><form method="post" action="register.php" onsubmit="return validateRegister(this);">

                <!-- First name -->
                <input name='firstName' type="text" value="<?php echo posted_value('firstName'); ?>"
                       onkeypress="hideElement('firstNameInvalid')"
                       autocomplete="given-name" placeholder="First Name" required>

                <!-- Display error message if first name contains number -->
                <span id="firstNameInvalid" class="errorMessage">
                    First name can only contain alphabets
                </span>
                <?php
                    if ($fNameInvalid) {
                        echo "<script>displayElement('firstnameInvalid', 'block');</script>";
                    }
                ?>

                <!-- Last name -->
                <input name='lastName' type="text" value="<?php echo posted_value('lastName'); ?>"
                       onkeypress="hideElement('lastNameInvalid')"
                       autocomplete="family-name" placeholder="Last Name" required>

                <!-- Display error message if last name contains number -->
                <span id="lastNameInvalid" class="errorMessage">
                    Last name can only contain alphabets
                </span>
                <?php
                    if ($lNameInvalid) {
                        echo "<script>displayElement('lastNameInvalid', 'block');</script>";
                    }
                ?>

                <!-- Email address -->
                <input name='email' type="email" value="<?php echo posted_value('email'); ?>"
                       onkeypress="hideElement('emailDuplicate')"
                       autocomplete="email" placeholder="Email Address" required>

                <!-- Display error message if the email is already registered in the database -->
                <span id="emailDuplicate" class="errorMessage">Email already exist</span>
                <?php
                    if ($emailDuplicate) {
                        echo "<script>displayElement('emailDuplicate', 'block');</script>";
                    }
                ?>

                <!-- Post code -->
                <input name="postCode" type="number" value="<?php echo posted_value('postCode'); ?>"
                       autocomplete="on" min="0" max="9999" placeholder="Post Code" required>

                <!-- Username -->
                <input name='username' type="text" value="<?php echo posted_value('username'); ?>"
                       onkeypress="hideElement('usernameDuplicate'); hideElement('usernameInvalid')"
                       autocomplete="username" placeholder="Username" required>

                <!-- Display error message if the username is already registered in the database -->
                <span id="usernameDuplicate" class="errorMessage">Username already exist</span>
                <?php
                    if ($usernameDuplicate) {
                        echo "<script>displayElement('usernameDuplicate', 'block');</script>";
                    }
                ?>

                <!-- Display error message if the username contains white space -->
                <span id="usernameInvalid" class="errorMessage">Username must not contain spaces</span>
                <?php
                    if ($usernameInvalid) {
                        echo "<script>displayElement('usernameInvalid', 'block');</script>";
                    }
                ?>

                <!-- Password -->
                <input name='password' type="password" value="<?php echo posted_value('password'); ?>"
                       autocomplete="new-password" placeholder="Password" required>

                <!-- Confirm password -->
                <input name='confirmPassword' type="password"
                       onkeypress="hideElement('passwordNotMatch')"
                       autocomplete="new-password" placeholder="Confirm Password" required>

                <!-- Display error message if password does not match with confirm password -->
                <span id="passwordNotMatch" class="errorMessage">Password does not match</span>

                <!-- Validates the form and submit -->
                <input type='submit' name="register" value='Register'>

            </form></div>

        </article>

    </div>

    <!-- Footer -->
    <?php include '../include/footer.php'; ?>

</body>

</html>