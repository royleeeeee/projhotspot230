<?php include '../public/php/loginPhp.php' ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Login - Wifi Hotspot</title>

    <?php include '../include/metadata.php'; ?>

    <link rel="stylesheet" href="../public/stylesheet/style.css"/>
    <link rel="shortcut icon" type="image/png" href="../public/image/favicon.png"/>

    <script src="../public/javascript/javascript.js"></script>

</head>

<body class='wrapperBody'>
    <?php include '../include/microdata.php' ?>

    <!-- Banner and Navigation Bar -->
    <?php
        $page = 'login';
        include '../include/navbar.php';
    ?>

    <div class='wrapperContent'>

        <article class="content">

            <!-- Page name -->
            <header><h2>Login</h2></header>

            <!-- Current directory -->
            <footer><p class='directory'>
                Login
            </p></footer>

            <!-- Login form -->
            <div><form method="POST" action="login.php">

                <!-- Enter username -->
                <input name='username' type="text" value="<?php echo posted_value('username'); ?>"
                       onkeypress="hideElement('loginFailed')"
                       autocomplete="username" placeholder="Username" required>

                <!-- Enter password -->
                <input name='password' type="password"
                       onkeypress="hideElement('loginFailed')"
                       autocomplete="current-password" placeholder="Password" required>

                <!-- Display an error message if username or password does not match with the database -->
                <span id="loginFailed" class="errorMessage">Username or password does not match</span>
                <?php
                    if (!$loginStatus) {
                        echo "<script>displayElement('loginFailed', 'block');</script>";
                    }
                ?>

                <!-- Matches provided credentials with user database and login  -->
                <input id="loginButton" type='submit' name="loginButton" value='Login'>

            </form></div>

            <!-- Links to registration page if user does not have an account -->
            <a id="registerPrompt" href="register.php">New user? Register here</a>

        </article>

    </div>

    <!-- Footer -->
    <?php include '../include/footer.php'; ?>

</body>

</html>