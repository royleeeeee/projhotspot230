<?php

include '../public/php/detailPhp.php';

foreach($arrayDetail as $detail) { # opening bracket for $arrayDetail loop

?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title><?php echo $detail['name'] ?> - Wifi Hotspot</title>

    <?php include '../include/metadata.php'; ?>

    <link rel="stylesheet" href="../public/stylesheet/style.css"/>
    <link rel="shortcut icon" type="image/png" href="../public/image/favicon.png"/>

    <script src="../public/javascript/javascript.js"></script>
    <script src="../public/javascript/mapFunctions.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
   integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
   crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
   integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
   crossorigin=""></script>

</head>

<body class='wrapperBody'>

    <?php include '../include/microdata.php' ?>

    <!-- Banner and Navigation Bar -->
    <?php
        $page = 'search';
        include '../include/navbar.php';
    ?>

    <div class='wrapperContent'>

        <article class="content">

            <!-- Location Name -->
            <header><h2><?php echo $detail['name']; ?></h2></header>

            <!-- Current directory -->
            <footer><p class='directory'>
                <a href='search.php'>Search</a> /
                <a href="#" onclick="javascript:history.go(-1)">Result</a> /
                <?php echo $detail['name']; ?>
            </p></footer>

            <div itemscope itemtype="http://schema.org/Place">

                <div class="mapTitle">Map</div>

                <!-- Leaflet Map with a marker on selected location -->
                <div id="leafletMap"></div>

                <?php echo"
                
                <script>
                    
                    var markerArray = [
                        [". $detail['latitude'] .", ". $detail['longitude'] .", \"". $detail['name'] ."\"]
                    ]
                    
                    initiateMap(markerArray);
                    
                </script>   
               
                "; ?>

                <div class="informationTitle">Location Information</div>

                <!-- Details of selected location -->
                <div class="wrapperDetailContent">

                    <p><span class="detailInformationHeader">Name:</span>
                    <span itemprop="name"><?php echo $detail['name']; ?> </span></p>

                    <div itemprop="address" itemscope itemtype="http://schema.org/PostalAddress">
                        <p><span class="detailInformationHeader">Address:</span>
                        <span itemprop="streetAddress"><?php echo $detail['address']; ?></span></p>

                        <p><span class="detailInformationHeader">Suburb:</span>
                        <span itemprop="addressLocality"><?php echo $detail['suburb']; ?></span></p>
                    </div>

                    <div itemprop="geo" itemscope itemtype="http://schema.org/GeoCoordinates">
                        <meta itemprop="latitude" content="<?php echo $detail['latitude']; ?>">
                        <meta itemprop="longitude" content="<?php echo $detail['longitude']; ?>">
                    </div>

                    <?php

                    if ($arrayReview->rowCount() != 0) {
                        # show average rating if review exists for selected location
                        $averageRating = getAverageRatingById($pdo, $_GET['id']);
                        echo "                    
                            <div itemprop=\"aggregateRating\" itemscope 
                                itemtype=\"http://schema.org/AggregateRating\">
                                
                                <meta itemprop=\"ratingCount\" content=\"". $arrayReview->rowCount() ."\">
                                <meta itemprop=\"bestRating\" content=\"5\">
                                <meta itemprop=\"worstRating\" content=\"1\">
                                <meta itemprop=\"ratingValue\" content=\"$averageRating\">
        
                                <p>
                                    <span class=\"detailInformationHeader\">Average Rating:</span>
        
                                    <img itemprop=\"image\" class=\"ratingStar\"
                                         alt=\" ". $averageRating ." out of 5 star rating\"
                                         src=\"../public/image/star". $averageRating .".png\">
                                </p>
                                
                            </div>                    
                        ";
                    }

                    ?>

                </div>

                <?php } # closing bracket for $arrayDetail loop ?>

                <div class="informationTitle">User Reviews</div>

                <!-- User reviews -->
                <div class="wrapperDetailContent">

                    <?php
                    if ($arrayReview->rowCount() != 0) {
                        foreach($arrayReview as $review) {
                            echo "                            
                                <div itemprop=\"review\" itemscope itemtype=\"http://schema.org/Review\">
                                    <span class=\"detailReviewName\"
                                          itemprop=\"author\">". $review['reviewname']."</span>
                                    <span class=\"detailReviewDate\"
                                          itemprop=\"datePublished\">". $review['date']."</span>
    
                                    <span itemprop=\"reviewRating\" itemscope itemtype=\"http://schema.org/Rating\">
                                        <meta itemprop=\"bestRating\" content=\"5\">
                                        <meta itemprop=\"worstRating\" content=\"1\">
                                        <meta itemprop=\"ratingValue\" content=\"". $review['rating']."\">
    
                                        <img itemprop=\"image\" class=\"ratingStar\"
                                             alt=\"". $review['rating']." out of 5 star rating\"
                                             src=\"../public/image/star". $review['rating'].".png\">
    
                                    </span>
    
                                    <span class=\"detailReviewText\" itemprop=\"reviewBody\">
                                        ". $review['content']."
                                    </span>
                                </div>                            
                            ";
                        }
                    } ?>

                    <div class="instructionText">Submit a Review</div>
                    <span id="notLoggedIn" class="errorMessage">User must be
                        <a href="login.php">logged in </a>
                        to submit a review</span>
                    <?php
                        if (!isset($_SESSION['isUser'])) {

                            echo "<script>displayElement('notLoggedIn', 'block');</script>";

                        } else {
                            echo "
                                <script>hideElement('notLoggedIn')</script>
                                <!-- Submit review form -->
                                <form method=\"post\" 
                                action=\"detail.php?id=". $_GET['id'] ."\"
                                onsubmit=\"return validateReview(this);\">
            
                                    <!-- Select star rating -->
                                    <select name=\"submitReviewRating\" 
                                        onclick=\"hideElement('reviewRatingBlank');\">
                                        
                                        <option value='' disabled selected hidden>Select a Star Rating</option>
                                        <option value='1'>1</option>
                                        <option value='2'>2</option>
                                        <option value='3'>3</option>
                                        <option value='4'>4</option>
                                        <option value='5'>5</option>
                                        
                                    </select>
                                    
                                    <span id=\"reviewRatingBlank\" class=\"errorMessage\">
                                        A rating must be selected
                                    </span>
            
                                    <!-- Enter text review -->
                                    <textarea name=\"submitReviewText\" placeholder=\"Enter your review\"></textarea>
                                                
                                    <input name=\"id\" type=\"hidden\" 
                                    value=\"". $_GET['id'] ."\">
            
                                    <!-- Check if rating selection is blank and submit form to server -->
                                    <input  type=\"submit\" name=\"submitReview\" value=\"Submit Review\">
                                </form>                                            
                            ";
                        }
                    ?>

                </div>

            </div>

        </article>

    </div>

    <!-- Footer -->
    <?php include '../include/footer.php'; ?>

</body>

</html>