<?php include '../public/php/resultPhp.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>

	<title>Result - Wifi Hotspot</title>

    <?php include '../include/metadata.php';?>

    <link rel="stylesheet" href="../public/stylesheet/style.css"/>
    <link rel="shortcut icon" type="image/png" href="../public/image/favicon.png"/>

    <script src="../public/javascript/javascript.js"></script>
    <script src="../public/javascript/mapFunctions.js"></script>

    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.3.1/dist/leaflet.css"
   integrity="sha512-Rksm5RenBEKSKFjgI3a41vrjkw4EVPlJ3+OiI65vTjIdo9brlAacEuKOiQ5OFh7cOI1bkDwLqdLw3Zg0cRJAAQ=="
   crossorigin=""/>
    <script src="https://unpkg.com/leaflet@1.3.1/dist/leaflet.js"
   integrity="sha512-/Nsx9X4HebavoBvEBuyp3I7od5tA0UzAxs+j83KgC8PU0kgB4XiK4Lfe4y4cgBtaRJQEIFCW+oC506aPT2L1zw=="
   crossorigin=""></script>

</head>

<body class='wrapperBody'>

    <?php include '../include/microdata.php' ?>

	<!-- Banner and Navigation Bar -->
    <?php
        $page = 'search';
        include '../include/navbar.php';
    ?>
	
	<div class='wrapperContent'>

        <article class="content">

            <!-- Page name -->
            <header><h2><?php echo $headerMessage; ?></h2></header>

            <!-- Current directory -->
            <footer><p class='directory'>
                <a href='search.php'>Search</a> /
                Result
            </p></footer>

            <div>

                <div class="mapTitle" id="resultMapTitle">Map</div>

                <!-- Leaflet map with markers for each location -->
                <div id="leafletMap"></div>

                <script>

                    // Push information from databse into an array and use it to place markers on the map
                    var markerArray = [
                        <?php foreach ($arrayMarker as  $result) {
                            echo "[". $result['latitude'] .", ". $result['longitude'] .", \"".
                                $result['name'] ."\", ". $result['id'] ."],";
                        } ?>
                    ]

                    // Generate a map with markers
                    initiateMap(markerArray, true, <?php  if ($geoLocate) echo $geoLat.", ".$geoLng; ?>);

                </script>

                <!-- Table listing all locations -->
                <table class="resultTable">

                    <tr>
                        <th class="numberColumn">#</th>
                        <th>Name</th>
                        <th>Address</th>
                        <th class="detailColumn">Details</th>
                    </tr>

                    <?php
                        $index = 1;
                        foreach ($arrayResult as $result) {
                            echo '      
                                <tr class="resultRow">
                                    <td class="numberColumn">' . $index .'</td>
                                    <td>'. $result['name'] .'</td>
                                    <td>'. $result['address'] . ', ' . $result['suburb'] .'</td>
                                                                        
                                    <td class="detailColumn">
                                        <a href='."detail.php?id=".$result['id'].' class="viewDetailButton">
                                            View Details
                                        </a>
                                    </td>
                                </tr>                                                        
                            ';
                            $index ++;
                        }
                    ?>

                </table>

            </div>

        </article>

	</div>

    <!-- Footer -->
    <?php include '../include/footer.php'; ?>

</body>

</html>