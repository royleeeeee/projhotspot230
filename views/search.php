<?php include '../public/php/searchPhp.php'; ?>

<!DOCTYPE html>
<html lang="en">

<head>

    <title>Search - Wifi Hotspot</title>

    <?php include '../include/metadata.php';?>

    <link rel="stylesheet" href="../public/stylesheet/style.css"/>
    <link rel="shortcut icon" type="image/png" href="../public/image/favicon.png"/>

    <script src="../public/javascript/javascript.js"></script>
    <script src="../public/javascript/mapFunctions.js"></script>

</head>

<body class='wrapperBody'>

    <?php include '../include/microdata.php' ?>

    <!-- Banner and Navigation Bar -->
    <?php
        $page = 'search';
        include '../include/navbar.php';
    ?>

    <div class='wrapperContent'>

        <article class="content">

            <!-- Page name -->
            <header><h2>Search</h2></header>

            <!-- Current directory -->
            <footer><p class='directory'>
                Search
            </p></footer>

            <div class="instructionText">Pick an option and click Search</div>

            <!-- Search by criteria form -->
            <div><form method="post" id='criteriaSearch' action="search.php"
                       onsubmit="return validateSearch (this);">

                <!-- Suburb of location -->
                <select name="suburb" type="text" id="searchSuburb"
                        onclick="hideElement('criteriaBlank'); hideElement('noResult');"
                        onchange="if (this.selectedIndex) {
                            disableElement('searchName'); disableElement('searchRating');
                        }">

                    <!-- Placeholder -->
                    <option value='' disabled selected hidden>Select a Suburb</option>

                    <!-- Get a list of suburbs from database and list each of them as an option -->
                    <?php foreach ($cleanArraySuburb as $suburb) {
                        echo '<option value='. urlencode($suburb) .'>'. $suburb .'</option>';
                    } ?>

                </select>

                <!-- Name of location -->
                <input name="name" type="text" id="searchName"
                       autocomplete="address-level2" placeholder="Name of location"
                       onkeypress="hideElement('criteriaBlank'); hideElement('noResult');
                            disableElement('searchSuburb'); disableElement('searchRating');">

                <!-- Minimum average rating of location -->
                <select name="rating" id="searchRating"
                        onclick="hideElement('criteriaBlank'); hideElement('noResult');"
                        onchange="if (this.selectedIndex) {
                            disableElement('searchSuburb'); disableElement('searchName');
                        }">

                    <!-- Placeholder -->
                    <option value='' disabled selected hidden>Minimum Average Star Rating</option>

                    <option value='1'>1</option>
                    <option value='2'>2</option>
                    <option value='3'>3</option>
                    <option value='4'>4</option>
                    <option value='5'>5</option>

                </select>

                <!-- Display error message if no search criteria is selected -->
                <span id="criteriaBlank" class="errorMessage">A criteria must be selected</span>
                <?php if ($criteriaBlank) {
                    echo "<script>displayElement('criteriaBlank', 'block');</script>";
                } ?>

                <!-- Display error message if SQL query returns no result -->
                <span id="noResult" class="errorMessage">Query returns no result, try again</span>
                <?php if ($noResult) {
                    echo "<script>displayElement('noResult', 'block');</script>";
                } ?>

                <!-- Validates the form and submit -->
                <input type='submit' name="criteriaButton" value='Search By Criteria'
                       onclick="hideElement('criteriaBlank')">

            </form></div>

            <br>

            <!-- Search by geolocation form -->
            <div><form method="post" id="geolocationSearch"  action="search.php" name="geoForm">

                <input type="submit" name="geoButton" value="Search by Current Location">

            </form></div>

            <!-- Initiate geolocation -->
            <?php if($geoLocate) echo '<script>geoLocate();</script>'; ?>

        </article>

    </div>

    <!-- Footer -->
    <?php include '../include/footer.php'; ?>

</body>

</html>