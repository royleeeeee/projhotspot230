<!-- Schema.org microdata markup for Google + -->
<div itemscope itemtype="http://schema.org/WebSite">
    <div itemscope itemtype="http://schema.org/Thing">
        <meta itemprop="name" content="Wifi Hotspot Searcher">
        <meta itemprop="description" content="A site that searches for nearby Wifi Hotspot">
        <meta itemprop="image" content="https://i.imgur.com/TVkESkw.png">
    </div>
</div>

<!-- Twitter Card microdata -->
<meta property="twitter:card" content="summary">
<meta property="twitter:site" content="@wifihotspotsearcher">
<meta property="twitter:title" content="WifiHotspotSearcher">
<meta property="twitter:description" content="A site that searches for nearby Wifi Hotspot">
<meta property="twitter:creator" content="@gotbreads">
<meta property="twitter:image:src" content="https://i.imgur.com/TVkESkw.png">

<!-- Open Graph microdata -->
<meta property="og:type" content="article">
<meta property="og:title" content="Wifi Hotspot Searcher">
<meta property="og:url" content="http://10.0.0.12:1337/views">
<meta property="og:image" content="https://i.imgur.com/TVkESkw.png">
<meta property="og:description" content="A site that searches for nearby Wifi Hotspot">
<meta property="og:site_name" content="WifiHotspotSearcher">
<meta property="article:author" content="Roy Lee">