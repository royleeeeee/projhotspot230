<header class='wrapperHeader'>

    <!-- Banner logo -->
    <a href='index.php'><img src="../public/image/bannerImage.png" alt="banner logo"></a>

    <!-- Navigation bar -->
    <nav><ul>
        <li class='<?php echo (($page=='search') ? 'active':''); ?>'>
            <a href='search.php'>Search</a>
        </li>

        <?php
        session_start();

        if (!isset($_SESSION['isUser'])) {
            echo '
                <li class="'. (($page=='login') ? 'active':'') .'">
                    <a href=\'login.php\'>Login</a>
                </li>
            ';
        } else {
            echo "
                <li>
                    <a href='../public/php/logout.php'>Logout as ". $_SESSION['username'] ."</a>
                </li>
            ";
        }

        ?>

    </ul></nav>

</header>
